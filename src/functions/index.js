import axios from 'axios'

const f = () => {
  const axiosToken = () => {
    let headers = { 'Content-Type': 'application/json' }
    const tokenUser = window.localStorage.getItem('auth_token')
    let token = ''
    if (tokenUser) {
      token = tokenUser
      headers = { 'x-access-token': token, 'Content-Type': 'application/json' }
    }
    axios.create({
      baseURL: process.env.API,
      headers: headers
    })
    axios.defaults.baseURL = process.env.API
    axios.defaults.headers.common['x-access-token'] = token
    axios.defaults.headers.post['Content-Type'] = 'application/json'

    return axios
  }


  return { axiosToken }
}

export default f
