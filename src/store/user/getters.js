export function getUser (state) {
  return state.user
}

export function getToken (state) {
  return state.token
}
