import f from "../../functions/index";
import { api } from "boot/axios";

export function addUser({ commit }, data) {
  return new Promise((resolve, reject) => {
    const { name, surname, email, cpf, phone, nickname, auth } = data;

    api
      .post("/auth/admin", { name, surname, email, cpf, phone, nickname, auth })
      .then((res) => {
        console.log(res.data);
        resolve(res);
      })
      .catch((error) => {
        console.log(error);
        reject(error.response);
      });
  });
}

export function loginUser({ commit }, data) {
  return new Promise((resolve, reject) => {
    console.log("login data: ", data);
    api
      .post(
        "/auth",
        { nickname: data.nickname },
        { headers: { pswdtk: data.password } }
      )
      .then((res) => {
        console.log(res);
        window.localStorage.setItem("auth_token", res.data.token);

        commit("SET_TOKEN", res.data.token || "");
        commit("SET_USER", res.data.user || {});
        resolve(res);
      })
      .catch((error) => {
        reject(error.response);
        console.log(error);
      });
  });
}

export function logoutUser({ commit }) {
  commit("SET_TOKEN", "");
  commit("SET_USER", {});
}
